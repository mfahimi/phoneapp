package com.mfahimi.phone.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mfahimi.phone.databinding.ItemDialNumberBinding
import com.mfahimi.phone.model.DialData

class DialInputAdapter(
    private val data: List<DialData>
) : RecyclerView.Adapter<DialInputAdapter.ViewHolder>() {
    var itemClickListener: ((DialData) -> Unit)? = null

    class ViewHolder(val binding: ItemDialNumberBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemDialNumberBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvTitle.text = data[position].title
        holder.binding.tvData.text = data[position].data.joinToString("")
        holder.binding.root.setOnClickListener {
            itemClickListener?.invoke(data[position])
        }
    }
}