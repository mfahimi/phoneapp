package com.mfahimi.phone.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.mfahimi.phone.adapter.ContactsAdapter
import com.mfahimi.phone.adapter.DialInputAdapter
import com.mfahimi.phone.databinding.FragmentHomeBinding
import com.mfahimi.phone.ext.compare
import com.mfahimi.phone.model.DialData
import com.mfahimi.phone.usecase.FilterStrategy
import com.mfahimi.phone.usecase.FilterStrategyImpl
import com.mfahimi.phone.util.ContactProvider
import com.mfahimi.phone.util.DialDataProvider

class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    override val viewBinding: FragmentHomeBinding by lazy { FragmentHomeBinding.inflate(layoutInflater) }
    private val filterStrategy: FilterStrategy = FilterStrategyImpl()

    private var filteredContacts = ContactProvider.getContacts()
    private val userInputData = ArrayList<CharArray>()
    private var userInputTitle = ""
    private val contactsAdapter = ContactsAdapter(filteredContacts)

    override fun initViews(view: View, savedInstanceState: Bundle?) = with(binding) {
        val rvData = DialDataProvider.createDialUiData()
        val adapter = DialInputAdapter(rvData)
        rvInput.layoutManager = GridLayoutManager(requireContext(), 3)
        rvInput.adapter = adapter
        adapter.itemClickListener = { updateUiState(it) }

        rvOutput.layoutManager = LinearLayoutManager(requireContext())
        rvOutput.adapter = contactsAdapter
        imgBack.setOnClickListener {
            resetState()
        }
    }

    private fun resetState() {
        userInputData.clear()
        filteredContacts.clear()
        filteredContacts.addAll(ContactProvider.getContacts())
        userInputTitle = ""
        binding.tvUserInput.text = userInputTitle
        contactsAdapter.notifyDataSetChanged()
    }

    private fun updateUiState(dialData: DialData) {
        lifecycleScope.launchWhenResumed {
            userInputData.add(dialData.data)
            userInputTitle = userInputTitle.plus(dialData.title)
            binding.tvUserInput.text = userInputTitle
            val filter = filterStrategy.filterDataBaseInput(userInputData, filteredContacts)
            filteredContacts.compare(filter) { _: List<String>, _: List<String>, removed: List<String> ->
                removed.forEach {
                    val index = filteredContacts.indexOf(it)
                    filteredContacts.removeAt(index)
                    contactsAdapter.notifyItemRemoved(index)
                }
            }
        }
    }

    companion object {
        fun newInstance() = HomeFragment()
    }
}