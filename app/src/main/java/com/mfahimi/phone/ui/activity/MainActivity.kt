package com.mfahimi.phone.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.mfahimi.phone.databinding.ActivityMainBinding
import com.mfahimi.phone.ui.fragment.HomeFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction()
            .replace(binding.fragmentContainer.id, HomeFragment.newInstance())
            .commit()
    }
}