package com.mfahimi.phone.util

import com.mfahimi.phone.model.DialData


object DialDataProvider {
   fun createDialUiData():List<DialData>{
      return listOf(
         DialData("1",charArrayOf()),
         DialData("2", KEYS[0]),
         DialData("3", KEYS[1]),
         DialData("4", KEYS[2]),
         DialData("5", KEYS[3]),
         DialData("6", KEYS[4]),
         DialData("7", KEYS[5]),
         DialData("8", KEYS[6]),
         DialData("9", KEYS[7]),
         DialData("*", charArrayOf()),
         DialData("0", charArrayOf()),
         DialData("#",charArrayOf()),
      )
   }
     private val KEYS = arrayOf(
        charArrayOf('A', 'B', 'C'),//2
        charArrayOf('D', 'E', 'F'),//3
        charArrayOf('G', 'H', 'I'),//4
        charArrayOf('J', 'K', 'L'),//5
        charArrayOf('M', 'N', 'O'),//6
        charArrayOf('P', 'Q', 'R', 'S'),//7
        charArrayOf('T', 'U', 'V'),//8
        charArrayOf('W', 'X', 'Y', 'Z')//9
    )
}