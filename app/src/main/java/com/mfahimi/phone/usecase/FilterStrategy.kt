package com.mfahimi.phone.usecase

interface FilterStrategy {
    suspend fun filterDataBaseInput(input: List<CharArray>, data: List<String>): List<String>
}