package com.mfahimi.phone.usecase

import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test

class FilterStrategyImplTest {

    private lateinit var filterStrategy: FilterStrategy

    @Before
    fun setUp() {
        filterStrategy = FilterStrategyImpl()
    }

    @Test
    fun inputMatchesAllData() = runBlocking<Unit> {
        assertThat(
            filterStrategy.filterDataBaseInput(listOf(charArrayOf('a', 'b', 'c')), listOf("ali", "reza", "hasan"))
        ).containsExactly("ali", "reza", "hasan")
    }

    @Test
    fun inputMatchesSomeData() = runBlocking<Unit> {
        assertThat(
            filterStrategy.filterDataBaseInput(listOf(charArrayOf('a', 'b', 'c'), charArrayOf('l', 'm', 'k')), listOf("ali", "reza", "hasan"))
        ).containsExactly("ali")
    }

    @Test
    fun inputDoesntMatchesAnyData() = runBlocking<Unit> {
        assertThat(
            filterStrategy.filterDataBaseInput(listOf(charArrayOf('a', 'b', 'c'), charArrayOf('j', 'f', 'c')), listOf("ali", "reza", "hasan"))
        ).isEmpty()
    }

}