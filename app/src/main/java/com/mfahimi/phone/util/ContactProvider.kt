package com.mfahimi.phone.util

object ContactProvider {
    fun getContacts() = arrayListOf(
        "mohammad",
        "ensan",
        "soheil",
        "reza",
        "ali",
        "naghi",
        "farshad",
        "booris",
        "bahram",
        "hasan",
        "ghasem",
        "john",
        "bill",
        "phill",
        "mac",
        "ehsan",
        "mohsen",
        "morteza",
        "gorge",
        "mahdi",
    )
}