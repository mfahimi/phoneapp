package com.mfahimi.phone.usecase

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class FilterStrategyImpl : FilterStrategy {

    override suspend fun filterDataBaseInput(input: List<CharArray>, data: List<String>): List<String> = withContext(Dispatchers.Default) {
        if (data.isEmpty())
            return@withContext emptyList()
        if (input.isEmpty())
            return@withContext data

        val itemsToSearch = combineUserInput(input)
        val result = ArrayList<String>()
        data.forEach out@{ contact ->
            itemsToSearch.forEach {
                if (contact.contains(it, true)) {
                    result.add(contact)
                    return@out
                }
            }
        }
        return@withContext result
    }

    private fun combineUserInput(userInput: List<CharArray>): List<String> {
        if (userInput.isEmpty())
            return emptyList()
        val result = ArrayList<String>()
        generatePermutations(userInput, result, 0, "")
        return result
    }

    private fun generatePermutations(userInput: List<CharArray>, result: ArrayList<String>, depth: Int, current: String) {
        if (depth == userInput.size) {
            result.add(current)
            return
        }
        for (i in userInput[depth].indices) {
            generatePermutations(userInput, result, depth + 1, current + userInput[depth][i])
        }
    }
}