package com.mfahimi.phone.model

data class DialData(val title: String, val data: CharArray)