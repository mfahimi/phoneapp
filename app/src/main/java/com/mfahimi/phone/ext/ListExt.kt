package com.mfahimi.phone.ext


fun <T> List<T>.compare(
    new: List<T>,
    result: (common: List<T>, added: List<T>, removed: List<T>) -> Unit
) {
    result(intersect(new).toList(), new.minus(this), minus(new))
}